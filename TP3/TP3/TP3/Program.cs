﻿using System;
using System.Globalization;


namespace TP3
{
    class Program
    {
        static void Main(string[] args)
        {
            IBankAccount current = new CurrentAccount();
            IBankAccount savings = new SavingAccount();
            CultureInfo.CurrentCulture = new CultureInfo("fr-FR");

            current.PayIn(500);
            savings.PayIn(500);
            Console.WriteLine($"Épargne : {savings.Balance:C}");
            Console.WriteLine($"Courrant : {current.Balance.ToString("C",CultureInfo.CurrentCulture)}");
            current.Withdraw(600);
            savings.Withdraw(600);
        }
    }
}
