﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TP3
{
    public class SavingAccount : IBankAccount
    {
        private decimal _balance;
        public decimal Balance
        {
            get => _balance;
            private set
            {
                if (value < 0)
                {
                    _balance = 0;
                }
                else {
                    _balance = value;
                }
                
            }
        }

        public void PayIn(decimal amount)
        {
            Balance += amount;
            Console.WriteLine($"Dépot de {amount} sur le compte d'épargne : Balance {Balance}");
        }

        public bool Withdraw(decimal amount)
        {
            if(amount < Balance)
            {
                Balance -= amount;
                Console.WriteLine($"Retrait de {amount} sur le compte d'épargne : Balance {Balance}");
            }
            else
            {
                Console.WriteLine("Retrait impossible pour cause de solde insuffisant");

            }
            return true;
        }
    }
}
