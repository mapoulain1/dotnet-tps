﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TP3
{
    public class CurrentAccount : IBankAccount
    {
        private decimal _balance;
        public decimal Balance { get => _balance; private set => _balance = value; }


        public void PayIn(decimal amount)
        {
            Balance += amount;
            Console.WriteLine($"Dépot de {amount} sur le compte courrant : Balance {Balance}");
        }

        public bool Withdraw(decimal amount)
        {
            Balance -= amount;
            Console.WriteLine($"Retrait de {amount} sur le compte courrant : Balance {Balance}");
            return true;
        }
    }
}
