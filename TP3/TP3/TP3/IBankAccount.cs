﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TP3
{
    public interface IBankAccount
    {
        decimal Balance { get; }
        void PayIn(decimal amount);
        bool Withdraw(decimal amount);
    }
}
