﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;

namespace TP6
{
    class LinkedList<T> : IEnumerable
    {
        public LinkedListNode<T> First { get; private set; }
        public LinkedListNode<T> Last { get; private set; }
        
        
        public LinkedListNode<T> AddLast (T obj)
        {
            var newNode = new LinkedListNode<T>(obj);
            if (First is null)
            {
                First = newNode;
                Last = First;
            }
            else
            {
                Last.Next = newNode;
                Last = newNode;
            }
            return newNode;
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            LinkedListNode<T> current = First;
            while (current != null)
            {
                yield return current.Value;
                current = current.Next;
            }


        }
    }
}
