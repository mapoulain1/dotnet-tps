﻿using System;
using System.Collections.Generic;

namespace TP6
{
    class Program
    {
        static void Main(string[] args)
        {
            var list1 = new LinkedList<int>();
            try
            {
                list1.AddLast(2);
                list1.AddLast(3);
                list1.AddLast(int.Parse("5"));
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
            foreach (var item in list1)
            {
                Console.WriteLine(item);
            }
        }
    }
}
