﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;

namespace TP5
{
    public class DocumentQueue<IDocument> : Queue<IDocument>
    {
     
        public void ListQueue()
        {
            foreach(var doc in this)
            {
                Console.WriteLine(doc);
            }
        }

        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();
            foreach (var doc in this)
            {
                sb.Append($"{doc}\n");
            }
            return sb.ToString();
        }

    }
}
