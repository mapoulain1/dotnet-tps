﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TP5
{
    public class Document : IDocument
    {
        public string Title { get ; set ; }
        public string Content { get; set ; }
        public Document()
        {

        }

        public Document(string title, string content)
        {
            this.Title = title;
            this.Content = content;
        }

        public override string ToString()
        {
            return $"{Title} {Content}"; 
        }

    }
}
