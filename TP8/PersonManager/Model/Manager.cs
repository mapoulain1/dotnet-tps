﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;

namespace Model
{
    public class Manager
    {

        public IEnumerable<Person> People { get => new ReadOnlyCollection<Person>(InternalPeople); }
        public IEnumerable<Person> Students { get => People.OfType<Student>(); }
        public IEnumerable<Person> Teachers { get => People.OfType<Teacher>(); }



        private List<Person> InternalPeople { get; }

        public Manager()
        {
       


            InternalPeople = new List<Person>
            {
                new Student("LY", "Sinaro", 22, "A212"),
                new Student("POULAIN", "Maxime", 21, "A212"),
                new Student("Baptiste", "VALLET", 2, "A212"),
                new Teacher("Michel", "BRÉSIL", 2, "B009")
            };

        }

        public void Add(Person person)
        {
            if (person != null)
            {
                InternalPeople.Add(person);
            }
        }


    }
}
