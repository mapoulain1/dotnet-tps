﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Model
{
    public class Teacher : Person
    {
        public string Subject { get; protected set; }

        public Teacher(string name, string surname, int age,string subject) : base(name, surname, age)
        {
            Subject = subject;
        }

        public override string ToString()
        {
            return base.ToString() + $" {Subject}";
        }
    }
}
