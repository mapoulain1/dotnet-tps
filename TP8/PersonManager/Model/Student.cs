﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Model
{
    public class Student : Person
    {

        public string Classroom { get; protected set; }

        public Student(string name, string surname, int age, string classroom) : base(name, surname, age)
        {
            Classroom = classroom;
        }

        public override string ToString()
        {
            return base.ToString() + $" {Classroom}";
        }
    }
}
