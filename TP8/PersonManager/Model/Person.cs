﻿using System;
using System.Diagnostics.CodeAnalysis;

namespace Model
{
    public abstract class Person
    {
        public int Age { get; protected set; }
        public string Name { get; protected set; }
        public string Surname { get; protected set; }



        public Person(string name, string surname, int age)
        {
            Name = name;
            Surname = surname;
            Age = age;
        }

        public override string ToString()
        {
            return $"{Name} {Surname} {Age}";
        }

    }
}
