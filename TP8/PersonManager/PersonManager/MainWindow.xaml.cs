﻿using System.Collections;
using System.Collections.Generic;
using System.Windows;
using Model;
using System.Linq;

namespace PersonManager
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private Manager PersonManager { get; }
        public IEnumerable<Person> People { get => PersonManager.People; }


        private bool isStudent;


        public MainWindow()
        {
            InitializeComponent();
            DataContext = this;
            PersonManager = new Manager();
            isStudent = true;
        }

        private void RadioButton_Student(object sender, RoutedEventArgs e)
        {
            subjectClassroomTextblock.Text = "Classe :";
            isStudent = true;
        }

        private void RadioButton_Teacher(object sender, RoutedEventArgs e)
        {
            subjectClassroomTextblock.Text = "Matière :";
            isStudent = false;
        }

        private void Button_AddPerson(object sender, RoutedEventArgs e)
        {
            if (isStudent)
                PersonManager.Add(new Student(nameTextBox.Text, surnameTextBox.Text, int.Parse(ageTextBox.Text), subjectClassroomTextblock.Text));

            else
                PersonManager.Add(new Teacher(nameTextBox.Text, surnameTextBox.Text, int.Parse(ageTextBox.Text), subjectClassroomTextblock.Text));

            personListView.ItemsSource = PersonManager.People;
        }

        private void StudentSelect_Click(object sender, RoutedEventArgs e)
        {
            personListView.ItemsSource = PersonManager.Students;
        }

        private void TeacherSelect_Click(object sender, RoutedEventArgs e)
        {
            personListView.ItemsSource = PersonManager.Teachers;
        }

        private void AllSelect_Click(object sender, RoutedEventArgs e)
        {
            personListView.ItemsSource = PersonManager.People;
        }

        private void Name_Click(object sender, RoutedEventArgs e)
        {            
            personListView.ItemsSource = new List<Person>(personListView.ItemsSource.Cast<Person>()).OrderBy(x => x.Name);
        }

        private void Surname_Click(object sender, RoutedEventArgs e)
        {
            personListView.ItemsSource = new List<Person>(personListView.ItemsSource.Cast<Person>()).OrderBy(x => x.Surname);

        }

        private void Age_Click(object sender, RoutedEventArgs e)
        {
            personListView.ItemsSource = new List<Person>(personListView.ItemsSource.Cast<Person>()).OrderBy(x => x.Age);

        }
    }
}
