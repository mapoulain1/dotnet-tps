﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TP2
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine(CalculSommeEntiers(1, 10));
            Console.WriteLine(CalculSommeEntiers(1, 100));
            Console.WriteLine(CalculMoyenne(new List<double> { 4, 5, 8, 9, 6 }));
            ProcessLists();
            Console.ReadLine();
        }

        private static int CalculSommeEntiers(int deb, int fin)
        {
            int counter = 0;
            for (int i = deb; i <= fin; i++)
            {
                counter += i;
            }

            return counter;
        }

        private static double CalculMoyenne(ICollection<double> doubles)
        {
            return doubles.Average();
        }

        private static void ProcessLists()
        {
            List<int> list3 = new List<int>();
            List<int> list5 = new List<int>();
            for (int i = 0; i < 100; i++)
            {
                if (i % 3 == 0)
                {
                    list3.Add(i);
                }
                if (i % 5 == 0)
                {
                    list5.Add(i);
                }

            }
            Console.WriteLine(Enumerable.Intersect(list3, list5).Sum());
        }

    }
}
