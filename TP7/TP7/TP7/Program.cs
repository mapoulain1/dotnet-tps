﻿using System;

namespace TP7
{
    public class Program
    {
        public static void Main(string[] args)
        {
            MeteoSimulator simulator = new MeteoSimulator(10000);

            
            Console.WriteLine("Subscribing to events");
            simulator.MeteoChanged += Simulator_MeteoChanged;
            simulator.MeteoSunnied += Simulator_MeteoSunnied;
            
            Console.WriteLine("Starting simulation ...");
            simulator.Start();


        }


        private static void Simulator_MeteoSunnied(object sender, MeteoEventArgs e)
        {
            Console.WriteLine("Meteo is now Sunny !");
        }

        private static void Simulator_MeteoChanged(object sender, MeteoEventArgs e)
        {
            Console.WriteLine($"Meteo changed, now : {e}");
        }
    }
}
