﻿using System;

namespace TP7
{
    public class MeteoEventArgs : EventArgs
    {
        public Weather Weather { get; set; }

        public MeteoEventArgs(Weather weather)
        {
            this.Weather = weather;
        }

        public override string ToString()
        {
            return Weather.ToString();
        }
    }
}
