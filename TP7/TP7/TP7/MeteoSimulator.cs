﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;

namespace TP7
{
    public class MeteoSimulator
    {
        public int NumberToGenerate { get; private set; }

        public delegate void MeteoEventHandler(object sender, MeteoEventArgs e);

        public event MeteoEventHandler MeteoChanged;
        public event MeteoEventHandler MeteoSunnied;

        public Weather Weather { get; private set; }

        public Random Random { get; }


        public MeteoSimulator(int numberToGenerate)
        {
            this.NumberToGenerate = numberToGenerate;
            Weather = Weather.Stormy;
            Random = new Random();
        }


        public void Start()
        {
            for (int i = 0; i < NumberToGenerate; i++)
            {
                double random = Random.NextDouble();
                Weather newWeather;

                if(random <= 0.05)
                    newWeather = Weather.Sunny;
                else if (random <= 0.5)
                    newWeather = Weather.Cloudy;
                else if (random <= 0.9)
                    newWeather = Weather.Rainy;
                else 
                    newWeather = Weather.Stormy;

                if(newWeather != Weather)
                {
                    MeteoChanged?.Invoke(this, new MeteoEventArgs(Weather));
                }
                if(newWeather == Weather.Sunny)
                {
                    MeteoSunnied?.Invoke(this, new MeteoEventArgs(Weather));
                }
                Weather = newWeather;
                Thread.Sleep(Random.Next(50, 3000));

            }

        }



    }
}
