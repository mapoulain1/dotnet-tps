﻿using System;

namespace TP1
{
    class Program
    {
        static void Main(string[] args)
        {
            var hour = DateTime.Now.Hour;
            var name = Environment.UserName;
            var day = DateTime.Now.DayOfWeek;



            if (hour > 9 && hour < 18 && day >= DayOfWeek.Monday && day <= DayOfWeek.Friday)
            {
                Console.Write($"Bonjour {name}, il est {hour}h !");
            }
            else if ((hour <= 9 || hour >= 18) && day >= DayOfWeek.Monday && day < DayOfWeek.Friday)
            {
                Console.Write($"Bonsoir {name}, il est {hour}h !");
            }
            else
            {
                Console.Write($"Bon weekend {name} !");
            }


            Console.ReadLine();



        }
    }
}
