﻿using System;

namespace TP4
{
    class Program
    {
        static void Main(string[] args)
        {
            Foo foo = new Foo();

            foo.TakeFoo(69);
            foo.TakeFoo(new Car());
            foo.TakeBar(69);

        }
    }
}
