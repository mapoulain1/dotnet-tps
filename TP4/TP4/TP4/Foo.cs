﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TP4
{
    public class Foo
    {
        public void TakeFoo(int integer)
        {
            Console.WriteLine($"Integer received : {integer}");
        }
    
        public void TakeFoo<T>(T obj)
        {
            Console.WriteLine($"Objects received : {obj.GetType()}");
        }

        public void TakeBar<T>(T obj)
        {
            TakeFoo(obj);
        }

    }

}
