﻿using System;
using System.Threading;
using System.Threading.Tasks;

namespace DysonV8
{
    public class Program
    {
        static void Main(string[] args)
        {
            var res = Client.GetAsync("https://stackoverflow.com/questions/14854878/creating-new-thread-with-method-with-parameter").Result;

            var links = Client.FindLinks(res);
            foreach (var link in links)
            {
                Console.WriteLine($"For : {link}");
                new Thread(() => Client.DownloadPage("output", link)).Start();
            }


        }
    }
}
