﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Linq;

namespace DysonV8
{

    public static class Client
    {
        private static HttpClient HttpClient { get; }
        static Client()
        {
            HttpClient = new HttpClient();
        }

        public static string Get(string url)
        {
            return GetAsync(url).Result;
        }

        public static async Task<string> GetAsync(string url)
        {
            try
            {
                var result = await HttpClient.GetAsync(url);
                return await result.Content.ReadAsStringAsync();
            }
            catch (Exception e)
            {
                return e.ToString();
            }
        }


        public static List<string> FindLinks(string file, bool onlyHttpLinks = true)
        {
            List<string> list = new List<string>();
            MatchCollection m1 = Regex.Matches(file, @"(<a.*?>.*?</a>)", RegexOptions.Singleline);
            foreach (Match m in m1)
            {
                string value = m.Groups[1].Value;
                string href;
                Match m2 = Regex.Match(value, @"href=\""(.*?)\""", RegexOptions.Singleline);
                if (m2.Success)
                {
                    href = m2.Groups[1].Value;
                    list.Add(href);
                }
            }
            list = list.Distinct().ToList();
            list.RemoveAll(x => x == "/" || x.Contains('#'));

            if (onlyHttpLinks)
                list.RemoveAll(x => !x.Contains("http", StringComparison.OrdinalIgnoreCase));
            

            return list;
        }

        public static void DownloadPage(string path, string url)
        {
            var page = GetAsync(url).Result;
            string filename = $"{url.Replace('/', '_').Replace(':', '_').Replace('?', '_')}.html";
            Console.WriteLine($"{(FileSaver.Save(path, filename, page) ? "Done" : "Failed")}");

        }


    }
}
