﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace DysonV8
{
    public static class FileSaver
    {

        public static bool Save(string path, string filemane, string content)
        {
            try
            {
                Directory.CreateDirectory(path);
                File.WriteAllText(Path.Combine(path,filemane), content);
                return true;
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                return false;
            }

        }

    }
}
